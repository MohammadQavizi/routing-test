import React from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom'
import About from './Pages/About';
import Home from './Pages/Home';
import NotFound from './Pages/NotFound';
const App = () => {
  return (
    <div>
      <Router>
        <Link to={"/Home"}>Home</Link>
        {"   "}
        <Link to={"/About"}>About</Link>
        {"   "}
        <Link to={"/Wrong"}>Wrong</Link>
        <hr />
        <Switch>
          <Route exact path={"/"} component={Home} />
          <Route exact path={"/Home"} component={Home} />
          <Route exact path={"/About"} component={About} />
          <Route path={"/*"} component={NotFound} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;